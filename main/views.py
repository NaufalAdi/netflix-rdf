from django.shortcuts import render
from django.http import HttpResponse
from graph_loader import GraphLoader
import sys
from SPARQLWrapper import SPARQLWrapper, JSON

# Create your views here.

endpoint_url = "https://query.wikidata.org/sparql"
prefix = """
PREFIX : <http://localhost:8000/> 
PREFIX bd: <http://www.bigdata.com/rdf#>
PREFIX cc: <http://creativecommons.org/ns#>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX geo: <http://www.opengis.net/ont/geosparql#>
PREFIX hint: <http://www.bigdata.com/queryHints#> 
PREFIX ontolex: <http://www.w3.org/ns/lemon/ontolex#>
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX prov: <http://www.w3.org/ns/prov#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX schema: <http://schema.org/>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>

PREFIX p: <http://www.wikidata.org/prop/>
PREFIX pq: <http://www.wikidata.org/prop/qualifier/>
PREFIX pqn: <http://www.wikidata.org/prop/qualifier/value-normalized/>
PREFIX pqv: <http://www.wikidata.org/prop/qualifier/value/>
PREFIX pr: <http://www.wikidata.org/prop/reference/>
PREFIX prn: <http://www.wikidata.org/prop/reference/value-normalized/>
PREFIX prv: <http://www.wikidata.org/prop/reference/value/>
PREFIX psv: <http://www.wikidata.org/prop/statement/value/>
PREFIX ps: <http://www.wikidata.org/prop/statement/>
PREFIX psn: <http://www.wikidata.org/prop/statement/value-normalized/>
PREFIX wd: <http://www.wikidata.org/entity/>
PREFIX wdata: <http://www.wikidata.org/wiki/Special:EntityData/>
PREFIX wdno: <http://www.wikidata.org/prop/novalue/>
PREFIX wdref: <http://www.wikidata.org/reference/>
PREFIX wds: <http://www.wikidata.org/entity/statement/>
PREFIX wdt: <http://www.wikidata.org/prop/direct/>
PREFIX wdtn: <http://www.wikidata.org/prop/direct-normalized/>
PREFIX wdv: <http://www.wikidata.org/value/>
PREFIX wikibase: <http://wikiba.se/ontology#>
"""


def search(request):  # pake filter contains
    title = request.GET.get('title', '')
    year = request.GET.get('year', '')
    genre = request.GET.get('genre', '')
    typ = request.GET.get('type', '')
    graph = GraphLoader().graph

    query = prefix + f"""
            SELECT DISTINCT ?uri ?type ?title (GROUP_CONCAT(?genre; separator=",") as ?genres) ?release_year ?imdb_score ?tmdb_score
            WHERE {{
                ?uri :title ?title ;
                    :genres/rdfs:label ?genre ;
                    :release_year ?release_year ;
                    {f"rdf:type :{typ.upper()} ;"  if typ != '' else '' }
                    {f":genres :{genre.lower()} ;"  if genre != '' else '' }
                    rdf:type/rdfs:label ?type.

                OPTIONAL {{
                    ?uri :imdb_score ?imdb_score .
                }}
                OPTIONAL {{
                    ?uri :tmdb_score ?tmdb_score .
                }}
                FILTER regex(?title, "{title}","i")
                FILTER regex(STR(?release_year), "{year}","i")
            }}
            GROUP BY ?title ?release_year ?imdb_score ?tmdb_score
            ORDER BY desc(?release_year) LIMIT 200
        """

    qres = graph.query(query)
    res = []
    for row in qres:
        res.append({"uri": row.uri.split('/')[-1],
                    "type": row.type,
                    "title": row.title,
                    "genres": row.genres.split(","),
                    "release_year": row.release_year,
                    "imdb_score": row.imdb_score,
                    "tmdb_score": row.tmdb_score
                    })

    context = {'rdf_data': res}

    return render(request, 'main/index.html', context)


def wikidata(request):
    query = """
        CONSTRUCT {
        ?item rdfs:label ?itemLabel;
                wdt:P57 ?director.
        } 
        WHERE {
        ?item wdt:P31 wd:Q2431196;
                wdt:P57 ?director;
        SERVICE wikibase:label { bd:serviceParam wikibase:language "en". }
        }
        LIMIT 20"""
    data = get_results(endpoint_url, query)
    context = {'rdf_data': data["results"]["bindings"]}

    return render(request, 'main/index.html', context)


def get_results(endpoint_url, query):
    user_agent = "Netflix-RDF Python/%s.%s" % (
        sys.version_info[0], sys.version_info[1])
    # TODO adjust user agent; see https://w.wiki/CX6
    sparql = SPARQLWrapper(endpoint_url, agent=user_agent)
    sparql.setQuery(query)
    sparql.setReturnFormat(JSON)
    return sparql.query().convert()
