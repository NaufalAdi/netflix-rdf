from django.apps import AppConfig


class InfoboxConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'infobox'
