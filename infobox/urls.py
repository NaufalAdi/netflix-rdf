from django.urls import path
from django.views.generic.base import RedirectView

from . import views

app_name = 'infobox'

urlpatterns = [
    path('infobox/movie/<str:mov>', views.infobox_movie, name='infobox'),
    path('infobox/artist/<str:a>', views.infobox_artist, name='infobox_artist'),
    path('', RedirectView.as_view(url='/search/'))
]
