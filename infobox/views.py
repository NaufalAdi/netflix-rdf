from django.shortcuts import render

# Create your views here.
from django.shortcuts import render
from django.http import HttpResponse
from graph_loader import GraphLoader
import sys
from SPARQLWrapper import SPARQLWrapper, JSON

# Create your views here.

endpoint_url = "https://query.wikidata.org/sparql"
prefix = """
PREFIX : <http://localhost:8000/> 
PREFIX bd: <http://www.bigdata.com/rdf#>
PREFIX cc: <http://creativecommons.org/ns#>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX geo: <http://www.opengis.net/ont/geosparql#>
PREFIX hint: <http://www.bigdata.com/queryHints#> 
PREFIX ontolex: <http://www.w3.org/ns/lemon/ontolex#>
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX prov: <http://www.w3.org/ns/prov#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX schema: <http://schema.org/>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>

PREFIX p: <http://www.wikidata.org/prop/>
PREFIX pq: <http://www.wikidata.org/prop/qualifier/>
PREFIX pqn: <http://www.wikidata.org/prop/qualifier/value-normalized/>
PREFIX pqv: <http://www.wikidata.org/prop/qualifier/value/>
PREFIX pr: <http://www.wikidata.org/prop/reference/>
PREFIX prn: <http://www.wikidata.org/prop/reference/value-normalized/>
PREFIX prv: <http://www.wikidata.org/prop/reference/value/>
PREFIX psv: <http://www.wikidata.org/prop/statement/value/>
PREFIX ps: <http://www.wikidata.org/prop/statement/>
PREFIX psn: <http://www.wikidata.org/prop/statement/value-normalized/>
PREFIX wd: <http://www.wikidata.org/entity/>
PREFIX wdata: <http://www.wikidata.org/wiki/Special:EntityData/>
PREFIX wdno: <http://www.wikidata.org/prop/novalue/>
PREFIX wdref: <http://www.wikidata.org/reference/>
PREFIX wds: <http://www.wikidata.org/entity/statement/>
PREFIX wdt: <http://www.wikidata.org/prop/direct/>
PREFIX wdtn: <http://www.wikidata.org/prop/direct-normalized/>
PREFIX wdv: <http://www.wikidata.org/value/>
PREFIX wikibase: <http://wikiba.se/ontology#>
"""


def infobox_movie(request, mov):
    graph = GraphLoader().graph
    query = prefix + f"""
    SELECT DISTINCT ?title ?type ?imdb_id ?release_year ?imdb_score ?tmdb_score ?runtime ?description ?age_rest ?seasons (GROUP_CONCAT(?genre; separator=",") as ?genres)
    WHERE {{
        :{mov} :title ?title ;
            :genres/rdfs:label ?genre ;
            :runtime ?runtime ;
            :description ?description ;
            :imdb_id ?imdb_id;
            rdf:type/rdfs:label ?type;
            :release_year ?release_year .

            OPTIONAL {{
                :{mov} :imdb_score ?imdb_score .
            }}
            OPTIONAL {{
                :{mov} :tmdb_score ?tmdb_score .
            }}
            OPTIONAL {{
                :{mov} :age_certification/rdfs:label ?age_rest .
            }}
            OPTIONAL {{
                :{mov} :seasons ?seasons .
            }}
    }}
    GROUP BY ?title ?release_year ?imdb_score ?tmdb_score ?runtime ?description ?age_rest ?seasons ?type ?imdb_id
    """

    qres_movie = graph.query(query)
    q_mov = []
    for r in qres_movie:
        q_mov.append(
            {"title": r.title,
             "type": r.type,
             "imdb_id": r.imdb_id,
             "release_year": r.release_year,
             "imdb_score": r.imdb_score,
             "tmdb_score": r.tmdb_score,
             "runtime": r.runtime,
             "description": r.description,
             "age_rest": r.age_rest,
             "seasons": r.seasons,
             "genres": r.genres.split(",")}
        )

    # get actors
    query = prefix + f"""
    SELECT DISTINCT ?a ?artist ?character ?netflixMedia WHERE {{
    ?a rdf:type :Human;
     :creditedIn [ 
       :hasCharacter ?character ;
       :netflixMedia :{mov} ;
       :role :ACTOR ;           
     ] ;
     :name ?artist .
    }}
    """
    qres_artist = graph.query(query)

    artist_lst = []
    for r in qres_artist:
        artist_lst.append({"a": str(r.a).split(
            "/")[-1], "artist": str(r.artist), "char": str(r.character)})

    # get directors
    query = prefix + f"""
    SELECT DISTINCT ?a ?director WHERE {{
    ?a rdf:type :Human;
     :creditedIn [ 
       :netflixMedia :{mov} ;
       :role :DIRECTOR ;           
     ] ;
     :name ?director .
    }}
    """
    qres_dir = graph.query(query)

    dir_lst = []
    for r in qres_dir:
        dir_lst.append({"a": str(r.a).split("/")[-1], "dir": str(r.director)})

    if len(dir_lst) == 0:
        dir_lst = None

    wikidata = None
    if len(q_mov) > 0:
        wikidata = get_wikidata_movie(q_mov[0]['imdb_id'])

    context = {
        'rdf_data': q_mov,
        'artists': artist_lst,
        'directors': dir_lst,
        'wikidata': wikidata
    }

    return render(request, 'infobox/infobox.html', context)


def infobox_artist(request, a):

    graph = GraphLoader().graph

    # get actor info
    query = prefix + f"""
    SELECT DISTINCT ?a ?actor ?mov ?movie_title WHERE {{
        :{a} rdf:type :Human;
            :creditedIn [ 
                :netflixMedia ?mov ;
                :role :ACTOR ;           
            ] ;
        :name ?actor .
  
        ?mov :title ?movie_title .
    }}
    """

    qres_artist = graph.query(query)

    artist_name = ""
    starred_movies = []

    for r in qres_artist:
        print(r.actor, r.mov, r.movie_title)

    for r in qres_artist:
        artist_name = str(r.actor)
        break

    for r in qres_artist:
        starred_movies.append({'mov': str(r.mov).split(
            "/")[-1], 'movie_title': str(r.movie_title)})

    if len(starred_movies) == 0:
        starred_movies = None

    # get director info
    query = prefix + f"""
    SELECT DISTINCT ?a ?director ?mov ?movie_title WHERE {{
        :{a} rdf:type :Human;
        :creditedIn [ 
            :netflixMedia ?mov ;
            :role :DIRECTOR ;           
        ] ;
        :name ?director .
  
        ?mov :title ?movie_title .
    }}
    """
    qres_dir = graph.query(query)

    directed_movies = []

    for r in qres_dir:
        print(r.director, r.mov, r.movie_title)

    for r in qres_dir:
        artist_name = str(r.director)
        break

    for r in qres_dir:
        directed_movies.append({'mov': str(r.mov).split(
            "/")[-1], 'movie_title': str(r.movie_title)})

    if len(directed_movies) == 0:
        directed_movies = None

    wikidata = get_wikidata_artist(artist_name)
    print(wikidata)
    if wikidata is not None and 'date_of_birth' in wikidata:
        wikidata['date_of_birth']['value'] = wikidata['date_of_birth']['value'].split('T')[
            0]

    context = {
        "artist_name": artist_name,
        "starred_movies": starred_movies,
        "directed_movies": directed_movies,
        "wikidata": wikidata
    }
    return render(request, 'infobox/artist_page.html', context)


def get_wikidata_artist(artist_name):
    user_agent = "Netflix-RDF Python/%s.%s" % (
        sys.version_info[0], sys.version_info[1])
    sparql = SPARQLWrapper(
        "https://query.wikidata.org/sparql", agent=user_agent)

    query = f"""
        SELECT DISTINCT ?item ?itemDescription ?date_of_birth ?image ?country_of_citizenship ?country_of_citizenshipLabel ?spouse ?spouseLabel ?sex_or_gender ?sex_or_genderLabel WHERE {{
        ?item ?label "{artist_name}"@en.
        {{?item wdt:P106 wd:Q28389.}}
        UNION
        {{?item wdt:P106 wd:Q33999.}}
        UNION
        {{?item wdt:P106 wd:Q10798782.}}
        UNION
        {{?item wdt:P106 wd:Q10800557.}}
        UNION
        {{?item wdt:P106 wd:Q245068.}}
        UNION
        {{  ?item wdt:P106 wd:Q2526255. }}
        UNION
        {{   ?item wdt:P106 wd:Q3282637. }}
        UNION
        {{   ?item wdt:P106 wd:Q2059704. }}
        UNION
        {{  ?item wdt:P106 wd:Q578109. }}
        OPTIONAL {{ ?item wdt:P569 ?date_of_birth. }}
        OPTIONAL {{ ?item wdt:P18 ?image. }}
        OPTIONAL {{?item wdt:P27 ?country_of_citizenship. }}
        SERVICE wikibase:label {{ bd:serviceParam wikibase:language "en". }}
        OPTIONAL {{ ?item wdt:P26 ?spouse. }}
        OPTIONAL {{ ?item wdt:P21 ?sex_or_gender. }}
        }}
        LIMIT 1
    """

    sparql.setQuery(query)
    sparql.setReturnFormat(JSON)
    res = sparql.query().convert()["results"]["bindings"]
    print(res)
    return res[0] if len(res) > 0 else None


def get_wikidata_movie(imdb_id):
    user_agent = "Netflix-RDF Python/%s.%s" % (
        sys.version_info[0], sys.version_info[1])
    sparql = SPARQLWrapper(
        "https://query.wikidata.org/sparql", agent=user_agent)
    print(imdb_id)
    query = f"""
        SELECT DISTINCT ?item ?itemLabel ?image ?country_of_origin ?country_of_originLabel ?language ?languageLabel WHERE {{
        ?item wdt:P345 "{imdb_id}".
        SERVICE wikibase:label {{ bd:serviceParam wikibase:language "en". }}
        OPTIONAL {{ ?item wdt:P18 ?image. }}
        OPTIONAL {{ ?item wdt:P154 ?image. }}
        OPTIONAL {{ ?item wdt:P495 ?country_of_origin. }}
        OPTIONAL {{ ?item wdt:P364 ?language. }}
        }}
        LIMIT 1
    """

    sparql.setQuery(query)
    sparql.setReturnFormat(JSON)
    res = sparql.query().convert()["results"]["bindings"]
    print(res)
    return res[0] if len(res) > 0 else None
