from django.shortcuts import render
from django.http import HttpResponse
from graph_loader import GraphLoader
import sys
from SPARQLWrapper import SPARQLWrapper, JSON

# Create your views here.

endpoint_url = "https://query.wikidata.org/sparql"
prefix = """
PREFIX : <http://localhost:8000/> 
PREFIX bd: <http://www.bigdata.com/rdf#>
PREFIX cc: <http://creativecommons.org/ns#>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX geo: <http://www.opengis.net/ont/geosparql#>
PREFIX hint: <http://www.bigdata.com/queryHints#> 
PREFIX ontolex: <http://www.w3.org/ns/lemon/ontolex#>
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX prov: <http://www.w3.org/ns/prov#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX schema: <http://schema.org/>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>

PREFIX p: <http://www.wikidata.org/prop/>
PREFIX pq: <http://www.wikidata.org/prop/qualifier/>
PREFIX pqn: <http://www.wikidata.org/prop/qualifier/value-normalized/>
PREFIX pqv: <http://www.wikidata.org/prop/qualifier/value/>
PREFIX pr: <http://www.wikidata.org/prop/reference/>
PREFIX prn: <http://www.wikidata.org/prop/reference/value-normalized/>
PREFIX prv: <http://www.wikidata.org/prop/reference/value/>
PREFIX psv: <http://www.wikidata.org/prop/statement/value/>
PREFIX ps: <http://www.wikidata.org/prop/statement/>
PREFIX psn: <http://www.wikidata.org/prop/statement/value-normalized/>
PREFIX wd: <http://www.wikidata.org/entity/>
PREFIX wdata: <http://www.wikidata.org/wiki/Special:EntityData/>
PREFIX wdno: <http://www.wikidata.org/prop/novalue/>
PREFIX wdref: <http://www.wikidata.org/reference/>
PREFIX wds: <http://www.wikidata.org/entity/statement/>
PREFIX wdt: <http://www.wikidata.org/prop/direct/>
PREFIX wdtn: <http://www.wikidata.org/prop/direct-normalized/>
PREFIX wdv: <http://www.wikidata.org/value/>
PREFIX wikibase: <http://wikiba.se/ontology#>
"""


def ranking_imdb(request):
    graph = GraphLoader().graph
    query_imdb = prefix + """
        SELECT DISTINCT ?uri ?title ?release_year ?imdb_score
        WHERE {
            ?uri :title ?title ;
                :release_year ?release_year ;

            OPTIONAL {
                ?uri :imdb_score ?imdb_score .
            }
        }
        ORDER BY desc(?imdb_score)
        LIMIT 100
    """

    qres_imdb = graph.query(query_imdb)

    res = []
    for row in qres_imdb:
        res.append({"uri": row.uri.split('/')[-1],
                    "title": row.title,
                    "release_year": row.release_year,
                    "score": row.imdb_score,
                    })
    context = {'rdf_data': res}

    return render(request, 'ranking.html', context)


def ranking_tmdb(request):
    graph = GraphLoader().graph
    query_tmdb = prefix + """
        SELECT DISTINCT ?uri ?title ?release_year ?tmdb_score
        WHERE {
            ?uri :title ?title ;
                :release_year ?release_year ;

            OPTIONAL {
                ?uri :tmdb_score ?tmdb_score .
            }
        }
        ORDER BY desc(?tmdb_score)
        LIMIT 100
    """

    qres_tmdb = graph.query(query_tmdb)
    res = []
    for row in qres_tmdb:
        res.append({"uri": row.uri.split('/')[-1],
                    "title": row.title,
                    "release_year": row.release_year,
                    "score": row.tmdb_score,
                    })
    context = {'rdf_data': res}

    return render(request, 'ranking.html', context)
