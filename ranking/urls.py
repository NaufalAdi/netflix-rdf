from django.urls import path

from . import views

app_name = 'ranking'

urlpatterns = [
    path('imdb', views.ranking_imdb, name='ranking_imdb'),
    path('tmdb', views.ranking_tmdb, name='ranking_tmdb'),
]
